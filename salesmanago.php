<?php
/**
 * Plugin Name: SALESmanago
 * Description: SALESmanago Marketing Automation integration for WooCommerce
 * Version: 1.0.1
 * Author: SALESmanago
 * Author URI: http://www.salesmanago.pl
 * License: GPL2
 */

/*  Copyright 2017  SALESmanago  (email : support@salesmanago.pl)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once dirname(__FILE__) . '/src/vendor/autoload.php';

define(SM_VIEWS_DIR, dirname(__FILE__)."/src/WP/views/");
define(SM_INCLUDES_DIR, dirname(__FILE__)."/src/WP/includes/");

use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;

require_once dirname(__FILE__) . '/src/SM/DependencyManagement/IoC.php';
require_once dirname(__FILE__) . '/src/SM/Entity/Settings.php';
use bhr\SM\DependencyManagement\IoC as Container;

use bhr\SM\Controller\CreateAccountController;
use bhr\SM\Controller\SalesManagoController;

if (!class_exists('SMClient')) {
    require_once dirname(__FILE__) . '/src/WP/includes/SMClient.class.php';
}

if (!class_exists('SalesManagoService')) {
    require_once dirname(__FILE__) . '/src/SM/Services/SalesManagoService.php';
}

if (!class_exists('SMUser')) {
    require_once dirname(__FILE__) . '/src/WP/includes/SMUser.class.php';
}

if (!class_exists('SMProduct')){
    require_once dirname(__FILE__) . '/src/WP/includes/SMProduct.class.php';
}

if (!class_exists('SMFunctions')) {
    require_once SM_INCLUDES_DIR.'SMFunctions.class.php';
}

if (!class_exists('SMWoocommerce')) {
    require_once SM_INCLUDES_DIR.'SmWoocommerce.php';
}

use bhr\src\WP\includes\SMClient as SMClient;
use bhr\src\WP\includes\SMUser as SMUser;
use bhr\src\WP\includes\SMProduct as SMProduct;

use bhr\src\WP\includes\SmWoocommerce;

$container      = Container::init();
$sm_client      = new SMClient(); //client salesmanago;
$sm_user        = new SMUser(); //monitoring object;
$sm_product     = new SMProduct();
$settings       = new Settings();
$sm_woocommerce = new SMWoocommerce();

$sm_client_data = $sm_client->getClient();

$container::register("create-account-settings", function () {
    $settings = new Settings();
    $settings
        ->setEndpoint($sm_client_data['end_point'], false)
        ->setClientId($sm_client_data['clientId'])
        ->setApiSecret($sm_client_data['apiKey'])
        ->setDefaultApiKey()
        ->setOwner($sm_client_data['owner']);
    return $settings;
});

//top menu node:
add_action('admin_bar_menu', 'toolbar_link_to_salesmanago', 999);

function toolbar_link_to_salesmanago($wp_admin_bar)
{
    $args = array(
        'id'    => 'toplevel_page_salesmanago',
        'title' => 'SALESmanago',
        'href'  => 'admin.php?page=salesmanago',
        'meta'  => array( 'class' => 'salemanago-toolbar-page')
    );
    $wp_admin_bar->add_node($args);
}

//add scripts
function salesmanago_script()
{
// Enqueue monitor script
    wp_enqueue_script('wc-monitor', plugins_url('js/monitor.js', __FILE__), array(), false, true);

    $options = get_option('pb_salesmanago_options');
    wp_localize_script('wc-monitor', '_smid', $options['pb_salesmanago_client_id']);
    wp_localize_script('wc-monitor', '_smEndpoint', $options['pb_salesmanago_end_point']);

    if (isset($_COOKIE['smclient'])) {
        wp_localize_script('wc-monitor', '_smContactId', strip_tags($_COOKIE['smclient']));
    }
}
add_action('wp_enqueue_scripts', 'salesmanago_script');


//add language
function salesmanago_language_init()
{
    load_plugin_textdomain('wco-salesmanago', false, dirname(plugin_basename(__FILE__)) . '/languages');
}
add_action('init', 'salesmanago_language_init');


//add sm admin menu:
require_once SM_INCLUDES_DIR.'smMenu.ini.php';


//register and define the settings
add_action('admin_init', 'pb_salesmanago_admin_init');
function pb_salesmanago_admin_init()
{
    register_setting(
        'pb_salesmanago_options',
        'pb_salesmanago_options'
        //'pb_salesmanago_validate_option'
    );

    add_settings_section(
        'pb_salesmanago_main',
        __('Settings', 'wco-salesmanago'),
        'pb_salesmanago_section_text',
        'salesmanago'
    );

    add_settings_field(
        'pb_salesmanago_client_id',
        __('Client ID', 'wco-salesmanago'),
        'pb_salesmanago_setting_client_id_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_api_secret',
        __('API Secret', 'wco-salesmanago'),
        'pb_salesmanago_setting_api_secret_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_contact_owner_email',
        __('Contact owner email', 'wco-salesmanago'),
        'pb_salesmanago_setting_email_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_end_point',
        __('Endpoint', 'wco-salesmanago'),
        'pb_salesmanago_setting_end_point_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_web_push_code',
        __('Web push code', 'wco-salesmanago'),
        'pb_salesmanago_setting_web_push_code',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_pop_up_code',
        __('Pop-up code', 'wco-salesmanago'),
        'pb_salesmanago_setting_pop_up_code',
        'salesmanago',
        'pb_salesmanago_main'
    );
//add_settings_field(
//        'pb_salesmanago_tags_register',
//        __('Tags after registration', 'wco-salesmanago'),
//        'pb_salesmanago_setting_tags_register_input',
//        'salesmanago',
//        'pb_salesmanago_main'
//        );
//
//add_settings_field(
//        'pb_salesmanago_tags_login',
//        __('Tags after login', 'wco-salesmanago'),
//        'pb_salesmanago_setting_tags_login_input',
//        'salesmanago',
//        'pb_salesmanago_main'
//        );
}

//generate section header
function pb_salesmanago_section_text()
{
    echo  '<p>'.__('All necessary information are available at client account SALESmanago in Settings => Integration', 'wco-salesmanago').'</p>';
}

//display and fill out the contact form
function pb_salesmanago_setting_client_id_input()
{
//download from the database values
    $options = get_option('pb_salesmanago_options');
    $sm_client_id = $options['pb_salesmanago_client_id'];
    //displays a form fields
    echo "<input id='pb_salesmanago_client_id' name='pb_salesmanago_options[pb_salesmanago_client_id]' "
    . "type='text' value='{$sm_client_id}' size=40 />";
}

function pb_salesmanago_setting_api_secret_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_api_secret = $options['pb_salesmanago_api_secret'];
    echo "<input id='pb_salesmanago_api_secret' name='pb_salesmanago_options[pb_salesmanago_api_secret]' "
    . "type='text' value='{$sm_api_secret}' size=40 />";
}

function pb_salesmanago_setting_email_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_email = $options['pb_salesmanago_contact_owner_email'];
    echo "<input id='pb_salesmanago_contact_owner_email' name='pb_salesmanago_options[pb_salesmanago_contact_owner_email]' "
    . "type='text' value='{$sm_email}' size=40 />";
}

function pb_salesmanago_setting_end_point_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_end_point = $options['pb_salesmanago_end_point'];
    echo "<input id='pb_salesmanago_end_point' name='pb_salesmanago_options[pb_salesmanago_end_point]' "
    . "type='text' value='{$sm_end_point}' size=40 />";
}

function pb_salesmanago_setting_web_push_code()
{
    $options = get_option('pb_salesmanago_options');
    $sm_web_push_code = $options['pb_salesmanago_web_push_code'];
    echo "<input id='pb_salesmanago_web_push_code' name='pb_salesmanago_options[pb_salesmanago_web_push_code]' "
    . "type='text' value='{$sm_web_push_code}' size=40 />";
}

function pb_salesmanago_setting_pop_up_code()
{
    $options = get_option('pb_salesmanago_options');
    $sm_pop_up_code = $options['pb_salesmanago_pop_up_code'];

    echo "<textarea id='pb_salesmanago_pop_up_code' name='pb_salesmanago_options[pb_salesmanago_pop_up_code]' "
    . "type='text'cols=43 rows='7'/>{$sm_pop_up_code}</textarea>";
}

//function pb_salesmanago_setting_tags_register_input() {
//    $options = get_option('pb_salesmanago_options');
//    $sm_tags = $options['pb_salesmanago_tags_register'];
//    echo"<input id='pb_salesmanago_tags_register' name='pb_salesmanago_options[pb_salesmanago_tags_register]' "
//    . "type='text' value='{$sm_tags}' size=40 /><br>"
//    . "<p>". __('Tags should be separated by comma without space ex: woocommerce_register,register', 'wco-salesmanago') ."</p>";
//}

//function pb_salesmanago_setting_tags_login_input() {
//    $options = get_option('pb_salesmanago_options');
//    $sm_tags = $options['pb_salesmanago_tags_login'];
//    echo "<input id='pb_salesmanago_tags_login' name='pb_salesmanago_options[pb_salesmanago_tags_login]' "
//    . "type='text' value='{$sm_tags}' size=40 /><br>"
//    . "<p>". __('Tags should be separated by comma without space ex: woocommerce_login,login', 'wco-salesmanago') ."</p>";
//}


// Create SALESmanago user on Register and Purchase with Register
function pb_salesmanago_create_user($user_id)
{
    global $sm_user, $sm_user_data, $settings;

    $sm_user_data = $sm_user->getUser($user_id);

    if ($settings->isActive()) {
        $smService = new SalesManagoService();
        $smService->contactUpsert($settings, $sm_user_data);
    }
}



add_action('woocommerce_checkout_update_user_meta', 'pb_salesmanago_create_user');
add_action('woocommerce_customer_save_address', 'pb_salesmanago_create_user');
add_action('profile_update', 'pb_salesmanago_create_user');
add_action('user_register', 'pb_salesmanago_create_user');



//Update SALESmanago user data on Login
function pb_salesmanago_login_user($user_login)
{
    global $sm_user, $settings;

    $sm_user_data = $sm_user->getUser('', 'login', $user_login);

    if ($settings->isActive()) {
        $smService = new SalesManagoService();
        $smService->contactUpsert($settings, $sm_user_data);
    }
}

add_action('wp_login', 'pb_salesmanago_login_user');

//set client id cookie on login
function pb_salesmanago_cookie_id($user_login)
{
    $pb_salesmanago_user_login = get_user_by('login', $user_login);
    $pb_salesmanago_user_id = $pb_salesmanago_user_login->ID;

    setcookie('pbsmcookieid', $pb_salesmanago_user_id, time() + 3600, '/', COOKIE_DOMAIN, false);
}

add_action('wp_login', 'pb_salesmanago_cookie_id');

//delete client id cookie on logout

function pb_salesmanago_cookie_id_delete()
{
    if (isset($_COOKIE['pbsmcookieid'])) {
        setcookie('pbsmcookieid', "", time()-3600, '/', COOKIE_DOMAIN, false);
    }
}

add_action('wp_logout', 'pb_salesmanago_cookie_id_delete');

//Add SALESmanago user external event on Purchase
function pb_salesmanago_purchase($order)
{
    global $woocommerce, $sm_product, $sm_user, $settings;

    $sm_user_data = $sm_user->getPurchaseNoAccount($order->id);
    $sm_products = $sm_product->getProductsFromOrder($order);

    if ($settings->isActive()) {
        $smService = new SalesManagoService();
        $smService->contactExtEvent($settings, SalesManagoService::EVENT_TYPE_PURCHASE, $sm_products, $sm_user_data);
    }
}

add_action('woocommerce_order_details_after_order_table', 'pb_salesmanago_purchase');

//Create SALESmanago user on Purchase without register
function pb_salesmanago_no_account_purchase_create_user($order)
{
    if (!is_user_logged_in()) {
        global $woocommerce, $sm_user, $smService, $settings, $sm_product;

        $sm_user_data = $sm_user->getPurchaseNoAccount($order->id);

        #setcookie('pbsmorderid', $order->id, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);

        $sm_products = $sm_product->getProductsFromOrder($order);

        if ($settings->isActive()) {
            $smService = new SalesManagoService();
            $smService->contactExtEvent($settings, SalesManagoService::EVENT_TYPE_PURCHASE, $sm_products, $sm_user_data);
        }
    }
}

add_action('woocommerce_order_details_after_order_table', 'pb_salesmanago_no_account_purchase_create_user', 0);

//Add SALESmanago user external event on Cart
function pb_salesmanago_add_to_cart($pb_cookie_id)
{
    global $woocommerce, $sm_product, $settings, $sm_user;

    $sm_user_data = $sm_user->getUser(get_current_user_id());
    $sm_products = $sm_product->getProductFromCart($woocommerce);

    if ($settings->isActive()) {
        $smService = new SalesManagoService();
        $smService->contactExtEvent($settings, SalesManagoService::EVENT_TYPE_CART, $sm_products, $sm_user_data);
    }
}

add_action('woocommerce_add_to_cart', 'pb_salesmanago_add_to_cart');

function post_user_sm_without_create_checkbox($order)
{
    $user_id =get_current_user_id();

    if ($user_id == 0 || $user_id == null) {
        pb_salesmanago_no_account_purchase_create_user($order);
        pb_salesmanago_purchase($order);
    }
}
add_action('woocommerce_order_details_after_order_table', 'post_user_sm_without_create_checkbox');

require_once SM_INCLUDES_DIR.'smTooltip.ini.php';
