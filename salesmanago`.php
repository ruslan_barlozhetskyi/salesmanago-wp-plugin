<?php
/**
 * Plugin Name: SALESmanago
 * Description: SALESmanago Marketing Automation integration for WooCommerce
 * Version: 1.0.1
 * Author: SALESmanago
 * Author URI: http://www.salesmanago.pl
 * License: GPL2
 */

/*  Copyright 2017  SALESmanago  (email : support@salesmanago.pl)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
require_once 'src/vendor/autoload.php';

require_once 'src/WP/includes/SmWoocommerce.php';

use bhr\src\WP\includes\SMClient;
use bhr\src\WP\includes\SMFunctions;
use bhr\src\WP\includes\SMProduct;
use bhr\src\WP\includes\SMUser;

use bhr\src\WP\includes\SmWoocommerce;

$sm_woocommerce = new SmWoocommerce($wpdb);

//get woocommerce customer:
$woorders = $sm_woocommerce->smGetCustomers();
var_dump($woorders);
