<div class="wrap">
    <h2>SALESmanago Marketing Automation | <?php echo __('Plugin Option', 'wco-salesmanago'); ?> </h2>
    <form action="options.php" method="post" enctype="multipart/form-data"> <?php settings_fields('pb_salesmanago_main'); ?>
        <?php settings_fields('pb_salesmanago_options'); ?>
        <?php do_settings_sections('salesmanago'); ?>
        <input name="Submit" type="submit" value="<?php echo __('Save', 'wco-salesmanago'); ?>" />
    </form>
</div>