<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.12.17
 * Time: 12:02
 */

namespace bhr\src\WP\includes;

use \DateTime as DateTime;

class SMProduct
{
    public $single;

    public function __construct()
    {
        $this->single = true;
    }

    public function getProductsFromOrder($order)
    {
        global $single;

        $dt = new DateTime('NOW');
        $items = $order->get_items();
        $payment_method = '_payment_method_title';
        $payment_method = get_post_meta($order->id, $payment_method, $single);

        /*
        $purchase_billing_email     = '_billing_email';
        $purchase_order_total       = '_order_total';
        $pb_salesmanago_purchase_billing_email  = get_post_meta($order->id, $purchase_billing_email, $single);
        $pb_salesmanago_purchase_order_total    = get_post_meta($order->id, $purchase_order_total, $single);
        */

        foreach ($items as $product) {
            $product_ids[] = $product['product_id'];
            $product_name[] = $product['name'];
            $product_total[] = $product['total'];
        }

        $location = $order->data['billing']['city'].", ";
        $location.= $order->data['billing']['state'].", ";
        $location.= $order->data['billing']['city'].", ";
        $location.= $order->data['billing']['address_1'].", ";
        $location.= $order->data['billing']['address_2'];

        $product = array(
            'date' => $dt->format('c'),
            'products' => implode(',', $product_ids),
            'description' => $payment_method,
            'detail1' => implode(',', $product_name),
            'value' => implode($product_total),
            'externalId' => $order->id,
            'location' => $location
        );

        return $product;
    }

    public function getProductFromCart(\WooCommerce $woocommerce)
    {
        $products_ids = array();
        $cart_total_price = 0;

        $dt = new DateTime('NOW');
        $pb_cart = $woocommerce->cart->get_cart();

        foreach ($pb_cart as $product) {
            $products_ids[] = $product['product_id'];
            $product_name[] = $product['name'];
            $cart_total_price = $cart_total_price + ($product['quantity']*$product['data']->price);
        }

        $products = array(
            'date' => $dt->format('c'),
            'products' => implode(',', $products_ids),
            'description' => 'Cart',
            //'detail1' => implode(',', $product_name),
            //'value' => implode(',',$cart_total_price),
            'value' => $cart_total_price,
        );

        return $products;
    }

}