<?php

namespace bhr\src\WP\includes;

class SmWoocommerce
{
    public function smGetCustomers($page = '0', $limit = '1000')
    {
        global $wpdb;

        $offset = $page * $limit;

        $sql = "SELECT ID as user_id, user_email ";
        $sql.= "FROM {$wpdb->prefix}users ";
        $sql.= "ORDER BY user_registered DESC ";
        $sql.= "LIMIT {$limit} OFFSET {$offset} ";

        $query = $wpdb->get_results($sql);

        foreach ($query as $result) {
            $result = (array)$result;
            $results[] = $result;
        }


        if (!isset($results)) {
            return false;
        }

        foreach ($results as $wkey => $user) {
            $meta = get_user_meta($user['user_id']);

            foreach ($meta as $mKey => $mVal) {
                $meta[$mKey] = $mVal[0];
            }

            $results[$wkey] = array_merge($results[$wkey], $meta);
        }

        $result = self::smTranslateCustomers($results);

        return $result;
    }

    public function smTranslateCustomers($customers = array())
    {
        $nUser = array();

        foreach ($customers as $user) {
            $bName = $user['billing_first_name'].' '.$user['billing_last_name'];
            $bName = empty($bName)? '' : $bName;

            $name = $user['first_name'].' '.$user['last_name'];
            $name = empty($name) ? $bName : $name;

            $streetAddress = $user['billing_address_1'].' '.$user['billing_address_2'];
            $streetAddress = empty($streetAddress) ? '' : $streetAddress;

            $nUser['email'] = empty($user['user_email']) ? $user['billing_email'] : $user['user_email'];
            $nUser['contact'] = array(
                'email' => $user['user_email'],
                'name' => $name,
                'phone'=> $user['billing_phone'],
                'fax'=>'',
                'company'=>'',
                'externalId'=>$user['user_id'],
                'address'=>array(
                    'streetAddress'=>$streetAddress,
                    'zipCode'=>$user['billing_phone'],
                    'city'=>$user['billing_city'],
                    'country'=>$user['billing_country']
                ),
            );
            $nUser['properties']=array(
                'custom.nickname'=>$user['nickname'],
                'custom.sex'=>''
            );
            $nUser['birthday'] = '';

            $result[] = $nUser;
        }

        return $result;
    }
}