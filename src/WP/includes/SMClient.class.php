<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.12.17
 * Time: 16:15
 */


namespace bhr\src\WP\includes;

class SMClient
{
    public function __construct()
    {
    }

    public function getClient()
    {
        $option = get_option('pb_salesmanago_options');

        $sm_client = array(
            'clientId'     =>$option['pb_salesmanago_client_id'],
            'apiKey'       =>$option['pb_salesmanago_api_secret'],
            'owner'        =>$option['pb_salesmanago_contact_owner_email'],
            'end_point'    =>$option['pb_salesmanago_end_point'],
            'tags_register'=>$option['pb_salesmanago_tags_register'],
            'tags_login'   =>$option['pb_salesmanago_tags_login'],
        );
        return $sm_client;
    }

    public function isLogged()
    {
        return true;
    }

    public function getIntegrations()
    {
        $integrations = array(
            'woocommerce'=> true,
            'contactFormSeven' => true
        );

        if (!empty($integrations)) {
            return $integrations;
        }

        return false;
    }
}