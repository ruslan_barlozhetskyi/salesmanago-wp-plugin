<?php

namespace bhr\SM\Services;

use GuzzleHttp\Client as GuzzleClient;
use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;

class SalesManagoService
{
    const METHOD_UPSERT = "/api/contact/upsert",
          METHOD_DELETE = "/api/contact/delete",
          METHOD_BATCH_UPSERT = "/api/contact/batchupsert",
          METHOD_ADD_EXT_EVENT = "/api/contact/addContactExtEvent",
          METHOD_BATCH_ADD_EXT_EVENT = "/api/contact/batchAddContactExtEvent",
          METHOD_UPDATE_EXT_EVENT = "/api/contact/updateContactExtEvent",
          METHOD_ACCOUNT_ITEMS = "/api/contact/updateContactExtEvent",

          EVENT_TYPE_CART = "CART",
          EVENT_TYPE_PURCHASE = "PURCHASE";

    /** @var GuzzleClient $guzzle */
    public $guzzle;

    /**
     * instantiate guzzle connection
     * @var Settings $settings
     * @return GuzzleClient
     */
    public function getGuzzleClient(Settings $settings)
    {
        if (!$this->guzzle) {
            $this->guzzle = new GuzzleClient([
                'base_uri' => $settings->getRequestEndpoint(),
                'verify' => false,
                'timeout'  => 5.0,
                'defaults' => [
                    'headers' => [
                        'Accept' => 'application/json, application/json',
                        'Content-Type' => 'application/json;charset=UTF-8'
                    ]
                ]
            ]);
        }
        return $this->guzzle;
    }

    public function checkConnection(Settings $settings)
    {
        try {
            $data = $this->__getDefaultApiData($settings);

            $guzzle = $this->getGuzzleClient($settings);

            $guzzleResponse = $guzzle->request('POST', self::METHOD_UPSERT, array(
                'json' => $data,
            ));

            $rawResponse = $guzzleResponse->getBody()->getContents();

            $response = json_decode($rawResponse, true);

            if (is_array($response)
                && array_key_exists('message', $response)
                && is_array($response['message'])
            ) {
                foreach ($response['message'] as $message) {
                    if ($message == 'Not authenticated') {
                        return false;
                    }
                }
                return true;
            } else {
                return null;
            }
        } catch (\Throwable $ex) {
            return null;
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $user
     * @param array $options
     * @param array $properties
     * @return array
     */
    public function contactUpsert(Settings $settings, $user = array(), $options = array(), $properties = array())
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'contact' => $this->__getContactData($user)
        ));

//        if (count($settings->getTags()) > 0) {
//            $data['tags'] = $settings->getTags();
//        }
//
//        if (count($settings->getRemoveTags()) > 0) {
//            $data['removeTags'] = $settings->getRemoveTags();
//        }

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (!empty($properties)) {
            foreach ($properties as $key => $value) {
                $data['properties'][$key] = $value;
            }
        }

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_UPSERT, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('contactId', $response)
            && $response['success'] == true
        ) {
            return $response['contactId'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to upsert contact: ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param string $userEmail
     * @return array
     */
    public function contactDelete(Settings $settings, $userEmail = '')
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'email' => $userEmail
        ));

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_DELETE, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to delete contact: ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param string $type
     * @param array $product
     * @param array $user
     * @param string $eventId
     * @return array
     */
    public function contactExtEvent(Settings $settings, $type, $product = array(), $user = array(), $eventId = "")
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'contactEvent' => $this->__getContactEventData($product, $type, $eventId)
        ));

        if (!empty($user)) {
            $data['email'] = $user['email'];
        }

        if (!empty($eventId)) {
            $method = self::METHOD_UPDATE_EXT_EVENT;
        } else {
            $method = self::METHOD_ADD_EXT_EVENT;
        }

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', $method, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('eventId', $response)
            && $response['success'] == true
        ) {
            return $response['eventId'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            if (!empty($eventId)) {
                $message = 'Unable to update contact external event. '. $message;
            } else {
                $message = 'Unable to add contact external event. '. $message;
            }
            throw new SalesManagoException($message);
        }
    }

    /**
     * @param array $user
     * @param array $options
     * @param array $properties
     * @return array
     */
    public function prepareContactsDetails(
        $user = array(),
        $options = array(),
        $properties = array()
    ) {
        $data = array(
            'contact' => $this->__getContactData($user)
        );

//        if (count($settings->getTags()) > 0) {
//            $data['tags'] = $settings->getTags();
//        }
//
//        if (count($settings->getRemoveTags()) > 0) {
//            $data['removeTags'] = $settings->getRemoveTags();
//        }

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (!empty($properties)) {
            foreach ($properties as $key => $value) {
                $data['properties'][$key] = $value;
            }
        }

        return $data;
    }


    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $upsertDetails
     * @return array
     */
    public function exportContacts(Settings $settings, $upsertDetails)
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'upsertDetails' => $upsertDetails
        ));

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_BATCH_UPSERT, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to batch export contacts: ' . $message);
        }
    }

    /**
     * @param string $type
     * @param array $product
     * @param array $user
     * @return array
     */
    public function prepareContactEvents($type, $product = array(), $user = array())
    {
        $data = array(
            'contactEvent' => $this->__getContactEventData($product, $type)
        );

        if (!empty($user['email'])) {
            $data['email'] = $user['email'];
        } elseif (!empty($user['contactId'])) {
            $data['contactId'] = $user['contactId'];
        }

        return $data;
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $events
     * @return array
     */
    public function exportContactExtEvents(Settings $settings, $events)
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'events' => $events
        ));

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_BATCH_ADD_EXT_EVENT, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to batch ext events export for contact: ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @return array
     */
    public function accountItems(Settings $settings)
    {
        $data = array(
            'token' => $settings->getToken(),
            'apiKey' => $settings->getApiKey()
        );

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_ACCOUNT_ITEMS, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to get account items '. $message);
        }
    }

    protected function __getDefaultApiData(Settings $settings)
    {
        $data = array(
            'clientId' => $settings->getClientId(),
            'apiKey' => $settings->getApiKey(),
            'requestTime' => time(),
            'sha' => $settings->getSha(),
            'owner' => $settings->getOwner()
        );
        return $data;
    }

    protected function __getContactData($user)
    {
        if (!empty($user)) {
            $contact = array();
            foreach ($user as $key => $value) {
                if (in_array($key, array("streetAddress", "zipCode", "city", "country"))) {
                    $contact["address"][$key] = $value;
                } else {
                    $contact[$key] = $value;
                }
            }
            return $contact;
        }
    }

    protected function __getContactEventData($product, $type, $eventId = '')
    {
        $contactEvent = array(
            'date' => 1000 * time(),
            'description' => $product['description'],
            'products' => $product['products'],
            'location' => $product['location'],
            'value' => $product['value'],
            'contactExtEventType' => $type,
            'detail1' => $product['detail1'],
            'detail2' => $product['detail1'],
            'detail3' => $product['detail1'],
        );

        if (!empty($eventId)) {
            $contactEvent['eventId'] = $eventId;
        }

        return $contactEvent;
    }

    protected function __handleError($rawResponse, $code)
    {
        if ($code >= 200 && $code < 500) {
            $array = json_decode($rawResponse, true);
            if ($array && array_key_exists('message', $array)) {
                $message = implode(' | ', $array['message']);
            } else {
                $message = 'No error message with response';
            }
        } else {
            $message = 'SALESmanago error '.$code.' with response';
        }
        return $message;
    }

}