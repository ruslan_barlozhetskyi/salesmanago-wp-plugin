<?php

namespace bhr\SM\Services;

use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;

class LoginAccountService extends BasicAccountService
{
    const METHOD_LOGIN_AUTHORIZE = "/api/authorization/token",
          METHOD_ACCOUNT_INTEGRATION = "/api/account/integration";

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $user
     * @return array
     */
    public function accessToken(Settings $settings, $user = array())
    {
        $data = array(
            'username' => $user['username'],
            'password' => $user['password'],
        );

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_LOGIN_AUTHORIZE, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('token', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to access token: ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @return array
     */
    public function integrationSettings(Settings $settings)
    {
        $data = array(
            'token' => $settings->getToken(),
            'apiKey' => $settings->getApiKey(),
        );

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_LOGIN_AUTHORIZE, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('shortId', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to access integration settings: ' . $message);
        }
    }

}