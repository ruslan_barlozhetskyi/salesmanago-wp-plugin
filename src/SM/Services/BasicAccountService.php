<?php

namespace bhr\SM\Services;

use GuzzleHttp\Client as GuzzleClient;
use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;

class BasicAccountService
{
    const METHOD_CREATE_LIVE_CHAT = "/api/wm/createLiveChat",
          METHOD_CREATE_BASIC_POPUP = "/api/wm/createBasicPopup",
          METHOD_CREATE_WEB_PUSH_CONSENT = "/api/wm/createWebPushConsentForm",
          METHOD_CREATE_WEB_PUSH_NOTIFICATION = "/api/wm/createWebPushNotification",
          METHOD_CREATE_WEB_PUSH_CONSENT_AND_NOTIFICATION = "/api/wm/createWebPushConsentFormAndNotification",
          METHOD_GET_INTEGRATION_PROPERTIES = '/api/account/integration/properties',
          METHOD_SET_INTEGRATION_PROPERTIES = '/api/account/integration/setProperties',

          REDIRECT_APP = "/api/authorization/authorize",
          REFRESH_TOKEN = "/api/account/authorize";

    /** @var GuzzleClient $guzzle */
    public $guzzle;

    /**
     * instantiate guzzle connection
     * @var Settings $settings
     * @return GuzzleClient
     */
    public function getGuzzleClient(Settings $settings)
    {
        if (!$this->guzzle) {
            $this->guzzle = new GuzzleClient([
                'base_uri' => $settings->getRequestEndpoint(),
                'verify' => false,
                'timeout'  => 5.0,
                'defaults' => [
                    'headers' => [
                        'Accept' => 'application/json, application/json',
                        'Content-Type' => 'application/json;charset=UTF-8'
                    ],
                ]
            ]);
        }
        return $this->guzzle;
    }

    protected function __getDefaultApiData(Settings $settings)
    {
        $data = array(
            'clientId' => $settings->getClientId(),
            'apiKey' => $settings->getApiKey(),
            'requestTime' => time(),
            'sha' => $settings->getSha(),
            'owner' => $settings->getOwner()
        );
        return $data;
    }

    protected function __getLiveChatData($options)
    {
        $data = array(
            "chat" => array(
                "name" => $options['chat-name'],
                "defaultConsultant" => array(
                    "name" => $options['consultant'],
                    "avatar" => array(
                        "url" => "https://s3-eu-west-1.amazonaws.com/salesmanago/chat/default_avatar.png"
                    )
                ),
                "contactOwner" => array(
                    "email" => $options['email']
                )
            )
        );
        return $data;
    }

    protected function __getBasicPopupData($options)
    {
        $data = array(
            "popup" => array(
                "settings" => array(
                    "confirmationEmailAccount" => array(
                        "email" => $options['email']
                    )
                ),
              "completedSteps" => 5,
              "active" => false
            )
        );
        return $data;
    }

    protected function __getWebPushConsentData($options)
    {
        $data = array(
            "webPushConsentForm" => array(
                "name" => "Formularz zgody 2017-11-15 16:07:14",
                "tags" => [],
                "consentForm" => array(
                    "title" => "Formularz zgody",
                    "body" => "Czy wyrażasz zgodę na otrzymywanie wiadomości Web Push?",
                    "imgUrl" => "https://s3-eu-west-1.amazonaws.com/salesmanagoimg/ye4vodnswfo6zp75/36m0iryqk4wlt6wu/vi3qhhiwqc485flt.png",
                    "thanksText" => "Witaj na pokładzie!",
                    "thanksUrl" => "https://www.domain.com/thanks_url"
                ),
                "buttonSettings" => array(
                    "confirmationBackgroundColor" => "#78a614",
                    "confirmationText" => "Tak",
                    "confirmationTextColor" => "#fff",
                    "rejectionText" => "Nie, dziękuję"
                ),
                "consentFormId" => null,
                "consentFormSize" => "MEDIUM",
                "marginTop" => 0,
                "active" => true
            )
        );
        return $data;
    }

    protected function __getWebPushNotificationData($options)
    {
        $data = array(
            "webPushNotification" => array(
                "name" => "Web Push 2017-11-15 16:08:24",
                "webPushNotification" => array(
                    "title" => "Witaj!",
                    "body" => "Mamy coś specjalnie dla Ciebie. Odwiedź naszą stronę.",
                    "imgUrl" => "https://s3-eu-west-1.amazonaws.com/salesmanagoimg/ye4vodnswfo6zp75/36m0iryqk4wlt6wu/vi3qhhiwqc485flt.png",
                    "targetUrl" => "https://www.domain.com/target_url"
                ),
                "richWebPush" => array(
                    "title" => null,
                    "body" => null,
                    "iconUrl" => null,
                    "targetUrl" => null,
                    "imageUrl" => null
                ),
                "receivers" => ".AllContacts",
                "sendingDate" => 1510704000000,
                "selectedConsentForms" => [123],
                "ttl" => array(
                    "timestamp" => "WEEKS",
                    "value" => 2419200
                ),
                "tags" => [],
                "excludedTags" => [],
                "webPushType" => "WEB_PUSH",
                "active" => true,
                "showWebPush" => true
            )
        );
        return $data;
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @return array
     */
    public function getIntegrationProperties(Settings $settings)
    {
        $data = array(
            "token" => $settings->getToken(),
            "clientId" => $settings->getClientId()
        );

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_GET_INTEGRATION_PROPERTIES, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return json_decode($response['properties'], true);
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to get properties: ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param string $properties
     * @return boolean
     */
    public function setIntegrationProperties(Settings $settings, $properties)
    {
        $data = array(
            "token" => $settings->getToken(),
            "clientId" => $settings->getClientId(),
            "properties" => $properties
        );

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_SET_INTEGRATION_PROPERTIES, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response['success'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to get properties: ' . $message);
        }
    }

    /**
     * @var Settings $settings
     * @return string
     */
    public function redirectApp(Settings $settings)
    {
        return $settings->getRequestEndpoint() . self::REDIRECT_APP . '?t=' . $settings->getToken();
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param string $method
     * @param array $options
     * @return array
     */
    public function createProduct(Settings $settings, $method, $options = array())
    {
        switch ($method) {
            case self::METHOD_CREATE_LIVE_CHAT:
                $productProperties = $this->__getLiveChatData($options);
                break;
            case self::METHOD_CREATE_BASIC_POPUP:
                $productProperties = $this->__getBasicPopupData($options);
                break;
            case self::METHOD_CREATE_WEB_PUSH_CONSENT:
                $productProperties = $this->__getWebPushConsentData($options);
                break;
            case self::METHOD_CREATE_WEB_PUSH_NOTIFICATION:
                $productProperties = $this->__getWebPushNotificationData($options);
                break;
            case self::METHOD_CREATE_WEB_PUSH_CONSENT_AND_NOTIFICATION:
                $productProperties = array_merge(
                    $this->__getWebPushConsentData($options),
                    $this->__getWebPushNotificationData($options)
                );
                break;
            default:
                throw new SalesManagoException('Unsupported data source type');
        }

        $data = array_merge(
            $this->__getDefaultApiData($settings),
            $productProperties
        );

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', $method, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);
        var_dump($response);
        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            $item = array(
                "success" => true,
            );
            return $item;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to add product: ' . $message);
        }
    }

    protected function __handleError($rawResponse, $code)
    {
        if ($code >= 200 && $code < 500) {
            $array = json_decode($rawResponse, true);
            if ($array && array_key_exists('message', $array)) {
                $message = implode(' | ', $array['message']);
            } else {
                $message = 'No error message with response';
            }
        } else {
            $message = 'SALESmanago error '.$code.' with response';
        }
        return $message;
    }
}