<?php

namespace bhr\SM\Services;

use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;

class CreateAppStoreAccountService extends BasicAccountService
{
    const METHOD_CREATE_ACCOUNT = "/api/account/registerAppstore",
          METHOD_CREATE_AUTHORIZE = "/api/account/authorize",
          METHOD_ADD_SUBSCRIBE_PRODUCTS = "/api/appstore/subscribeProducts",
          PRODUCT = array("EMAIL_MARKETING","LIVE_CHAT", "WEB_PUSH", "CF_P_LP");


    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $user
     * @return array
     */
    public function refreshToken(Settings $settings, $user = array())
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'email' => $user['email'],
        ));

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_CREATE_AUTHORIZE, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('token', $response)
            && !empty($response['token'])
        ) {
            return $response['token'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to refresh token:  ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $user
     * @param array $modules
     * @return array
     */
    public function createAccount(Settings $settings, $user, $modules)
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'email' => $user['email'],
            'password' => $user['password'],
            'lang' => $user['lang'],
            'items' => $modules
        ));

        if (isset($user['tags'])) {
            $data['tags'] = $user['tags'];
        }

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_CREATE_ACCOUNT, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response;
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to create account: ' . $message);
        }
    }

    /**
     * @throws SalesManagoException
     * @var Settings $settings
     * @param array $modules
     * @return array
     */
    public function addSubscribeProducts(Settings $settings, $modules)
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'items' => $modules
        ));

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_CREATE_AUTHORIZE, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && $response['success'] == true
        ) {
            return $response['success'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
            throw new SalesManagoException('Unable to add product: ' . $message);
        }
    }

}