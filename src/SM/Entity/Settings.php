<?php

namespace bhr\SM\Entity;

class Settings
{
    /**
     * @var boolean
     */
    protected $active = false;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $apiSecret;

    /**
     * @var string
     */
    protected $owner;

    /**
     * @var string
     */
    protected $sha;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var array
     */
    protected $tags;

    /**
     * @var array
     */
    protected $removeTags;

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getRequestEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        if (preg_match("@^(?:http:\/\/)([^/]+)@i", $this->endpoint, $matches)) {
            return $matches[1];
        }
    }

    /**
     * @param string $endpoint
     * @param boolean $ssl
     * @return $this
     */
    public function setEndpoint($endpoint, $ssl = true)
    {
        if (preg_match('/^https?:\/\//', $endpoint)) {
            if ($ssl === true && preg_match("@^(?:http:\/\/)([^/]+)@i", $endpoint, $matches)) {
                $this->endpoint = "https://" . $matches[1];
            } else {
                $this->endpoint = $endpoint;
            }
        } else {
            if ($ssl === true) {
                $this->endpoint = "https://" . $endpoint;
            } else {
                $this->endpoint = "http://" . $endpoint;
            }
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return $this
     */
    public function setDefaultApiKey()
    {
        $this->apiKey = md5(time() . get_class($this));
        return $this;
    }

    /**
     * @return string
     */
    protected function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @param string $apiSecret
     * @return $this
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;
        return $this;
    }

    /**
     * @param string $sha
     * @return $this
     */
    public function setSha($sha)
    {
        $this->sha = $sha;
        return $this;
    }

    public function generateSha()
    {
        $this->sha = sha1($this->getApiKey().$this->getClientId().$this->getApiSecret());
        return $this;
    }

    /**
     * @return string
     */
    public function getSha()
    {
        if (empty($this->sha)) {
            self::generateSha();
        }
        return $this->sha;
    }

    /**
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return implode(', ', $this->tags);
    }

    /**
     * @param string $tags
     */
    public function setTags($tags)
    {
        $tags = explode(',', $tags);
        foreach ($tags as $key => $tag) {
            $tags[$key] = trim($tag);
        }
        $this->tags = $tags;
    }

    /**
     * @param string $removeTags
     */
    public function setRemoveTags($removeTags)
    {
        $removeTags = explode(',', $removeTags);
        foreach ($removeTags as $key => $tag) {
            $removeTags[$key] = trim($tag);
        }
        $this->removeTags = $removeTags;
    }

    /**
     * @return array
     */
    public function getRemoveTags()
    {
        return implode(', ', $this->removeTags);
    }

}
