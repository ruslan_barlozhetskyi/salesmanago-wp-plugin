<?php

namespace bhr\src\SM\Controller;

use bhr\SM\Entity\Settings;
use bhr\SM\Services\CreateAppStoreAccountService;
use bhr\SM\Exception\SalesManagoException;
use bhr\SM\DependencyManagement\IoC as Container;

class CreateAccountController
{
    private $settings;
    private $service;

    public function __construct(Settings $settings)
    {
        $this->service = new CreateAppStoreAccountService();
        $this->settings = $settings;
    }

    public function refreshToken($user)
    {
        try {
            $this->settings->setToken(
                $this->service->refreshToken($this->settings, $user)
            );
        } catch (SalesManagoException $e) {
            echo $e->getMessage();
        }
    }

    public function createAccount($user, $modulesId)
    {
        try {
            $userData = $this->service->createAccount(
                $this->settings,
                $user,
                $this->__getModulesData($modulesId)
            );

            $userData['endpoint'] = $this->settings->getEndpoint();

            if (isset($user['email'])) {
                $userData['email'] = $user['email'];
            }

            $container = Container::init();

            $container::register("user-settings", function () use ($userData) {
                $settings = new Settings();
                $settings
//                    ->setEndpoint($userData['endpoint'])
                    ->setClientId($userData['clientId'])
                    ->setApiSecret($userData['apiSecret'])
                    ->setOwner($userData['email'])
                    ->setToken($userData['token']);
                return $settings;
            });

            $this->settings = $container::resolve("user-settings");
        } catch (SalesManagoException $e) {
            echo $e->getMessage();
        }
    }

    public function addSubscribeProducts($modulesId)
    {
        try {
            $this->service->addSubscribeProducts(
                $this->settings,
                $this->__getModulesData($modulesId)
            );
        } catch (SalesManagoException $e) {
            echo $e->getMessage();
        }
    }

    protected function __getModulesData($modulesId)
    {
        $modules = array();

        foreach ($modulesId as $value) {
            $obj = array("name" => CreateAppStoreAccountService::PRODUCT[$value]);

            if ($value == 0) {
                $obj = array_merge($obj, array(
                    "contactLimit"=> 1000
                ));
            }
            array_push($modules, $obj);
        }

        return $modules;
    }
}