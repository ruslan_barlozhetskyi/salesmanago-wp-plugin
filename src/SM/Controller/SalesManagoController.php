<?php

namespace bhr\SM\Controller;

use bhr\SM\Entity\Settings;
use bhr\SM\Services\SalesManagoService;
use bhr\SM\Exception\SalesManagoException;

class SalesManagoController
{
    const COOKIES_CLIENT = "smclient",
          COOKIES_EXT_EVENT = "smevent";

    private $settings;
    private $service;

    private function createCookie($name, $value)
    {
        $period = time() + (3600 * 86400);
        setcookie($name, $value, $period, '/');
    }

    public function __construct(Settings $settings)
    {
        $this->service = new SalesManagoService();
        $this->settings = $settings;
    }

    public function contactUpsert($user, $options, $properties)
    {
        try {
            $clientId = $this->service->contactUpsert($this->settings, $user, $options, $properties);
            self::createCookie(self::COOKIES_CLIENT, $clientId);
            return $clientId;
        } catch (SalesManagoException $e) {
            echo $e->getCode() . PHP_EOL;
        }
    }

    public function accountItems()
    {
        try {
            $response = $this->service->accountItems($this->settings);
            return json_encode($response);
        } catch (SalesManagoException $e) {
            echo $e->getCode() . PHP_EOL;
        }
    }

    public function exportContacts()
    {
        $data = array();
        $users = array();
        $options = array();
        $properties = array();

        foreach ($users as $user) {
            array_push(
                $data,
                $this->service->prepareContactsDetails($user, $options, $properties)
            );
        }
        try {
            $response = $this->service->exportContacts($this->settings, $data);
            return json_encode($response);
        } catch (SalesManagoException $e) {
            echo $e->getCode() . PHP_EOL;
        }
    }

    public function exportContactExtEvents()
    {
        $data = array();
        $orders= array();
        $user = array();

        foreach ($orders as $order) {
            array_push(
                $data,
                $this->service->prepareContactEvents(SalesManagoService::EVENT_TYPE_CART, $order, $user)
            );
        }
        try {
            $response = $this->service->exportContactExtEvents($this->settings, $data);
            return json_encode($response);
        } catch (SalesManagoException $e) {
            echo $e->getCode() . PHP_EOL;
        }
    }

    public function createMonitorVisitorsCode()
    {
        $code = "<script type=\"text/javascript\">
var _smid = '" . $this->settings->getClientId() . "';
(function(w, r, a, sm, s ) {
    w['SalesmanagoObject'] = r;
    w[r] = w[r] || function () {( w[r].q = w[r].q || [] ).push(arguments)};
    sm = document.createElement('script');
    sm.type = 'text/javascript'; sm.async = true; sm.src = a;
    s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(sm, s);
 })(window, 'sm', ('https:' == document.location.protocol ? 'https://' : 'http://') 
 + '" . $this->settings->getEndpoint() . "/static/sm.js');
                </script>";
        return $code;
    }

}