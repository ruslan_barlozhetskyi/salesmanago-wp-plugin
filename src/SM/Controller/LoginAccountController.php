<?php

namespace bhr\SM\Controller;

use bhr\SM\Entity\Settings;
use bhr\SM\Services\LoginAccountService;
use GuzzleHttp\Exception\ClientException;
use bhr\SM\Exception\SalesManagoException;
use bhr\SM\DependencyManagement\IoC as Container;

class LoginAccountController
{
    private $settings;
    private $service;

    public function __construct()
    {
        $this->service = new LoginAccountService();

        $container = Container::init();

        $container::register("user-settings", function () {
            $settings = new Settings();
            $settings
                ->setEndpoint("pre.salesmanago.pl");
            return $settings;
        });

        try {
            $this->settings = $container::resolve("user-settings");
        } catch (SalesManagoException $e) {
            echo $e->getMessage();
        }
    }

    public function accessToken($user)
    {
        try {
            $responseData = $this->service->accessToken($this->settings, $user);

            $this->settings
                ->setOwner($user['username'])
                ->setApiKey($user['username'])
                ->setToken($responseData['token'])
                ->setEndpoint($responseData['endpoint']);

            self::integrationSettings();
        } catch (SalesManagoException $e) {
            echo $e->getCode() .PHP_EOL;
        }
    }

    protected function integrationSettings()
    {
        try {
            $responseData = $this->service->integrationSettings($this->settings);

            $this->settings
                ->setClientId($responseData['shortId'])
                ->setSha($responseData['sha1']);
        } catch (SalesManagoException $e) {
            echo $e->getMessage();
        }
    }
}