<?php

require_once 'vendor/autoload.php';

use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;
use bhr\SM\DependencyManagement\IoC as Container;
use bhr\SM\Controller\CreateAccountController;
use bhr\SM\Controller\SalesManagoController;

$container = Container::init();

/*
$container::register("create-account-settings", function () {
    $settings = new Settings();
    $settings
        ->setEndpoint("app2.salesmanago.pl")
        ->setClientId("8da75fbd-46bd-4a38-8893-d0219d23e75c")
        ->setApiSecret("52548bb9-ba43-11e7-a157-0cc47a1254ce")
        ->setOwner("slawomir.chochorek@salesmanago.pl");
    return $settings;
});
*/

$container::register("create-account-settings", function () {
    $settings = new Settings();
    $settings
        ->setEndpoint("pre.salesmanago.pl", false)
        ->setClientId("5eca91e1-b8ab-11e7-adbf-0cc47a6bceb8")
        ->setApiSecret("52548bb9-ba43-11e7-a157-0cc47a1254ce")
        ->setDefaultApiKey()
        ->setOwner("appstore.partner@test.pl");
    return $settings;
});

$settings = null;
try {
    $settings = $container::resolve("create-account-settings");
} catch (SalesManagoException $e) {
    echo $e->getMessage();
}
$account = new CreateAccountController($settings);

$user = array(
    'email' => 'test0101@test.pl',
    'password' => 'test1234',
    'lang' => 'PL',
);

$modules = ['0'];

$account->createAccount($user, $modules);

$modules = ['1','2'];

$account->addSubscribeProducts($modules);

//
//$account->refreshUserToken($user);

try {
    $sss = $container::resolve("user-settings");
    var_dump($sss);
} catch (SalesManagoException $e) {
    echo $e->getMessage();
}